import { render } from "lit-html";
import { MessageList } from "./../templates/message-list";
import { errorInfo } from "../templates/error-info";
import { InfiniteList } from "./infinite-list";
import { fetchMessages } from "../services/messages.service";

/**
 * Main app class which holds data in state and render list elements according to given pointers
 * from infinite list component
 */
class App {
  set state(newState) {
    this._state = Object.assign({}, this._state, newState);
  }

  get state() {
    return this._state;
  }

  constructor() {
    this.messagesListContainer = document.querySelector(".main-section");
    this.messagesList = new MessageList({
      onElementRemove: this.handleRemoveElement.bind(this)
    });
    this.state = {
      isFetching: false,
      lastPageToken: "",
      firstItemIndex: 0,
      lastItemIndex: 20,
      data: []
    };
    this.renderElements();

    this.messagesListEl = document.querySelector(".messages-list");
    this.onInfiniteListRender = this.onInfiniteListRender.bind(this);

    new InfiniteList({
      containerElement: this.messagesListEl,
      minNodesLength: this.state.lastItemIndex,
      renderHandler: this.onInfiniteListRender
    });
  }

  /**
   * Handler invoked on item indexes change
   * @param {*} pointers
   */
  onInfiniteListRender(pointers) {
    const { isFetching } = this.state;
    // do not perform any new fetches if there is one pending
    if (!isFetching) {
      this.state = pointers;
      if (this.shoudStartNewRequest(pointers.lastItemIndex)) {
        this.fetchData().then(() => this.renderElements());
      }
      this.renderElements();
    }
  }

  renderElements() {
    const { firstItemIndex, lastItemIndex, data } = this.state;

    this.messagesList.setMessages(data.slice(firstItemIndex, lastItemIndex));
    render(this.messagesList.render(), this.messagesListContainer);
  }

  handleRemoveElement(id) {
    const { data } = this.state;

    let itemIndexToRemove = data.findIndex(item => item.id === parseInt(id));

    if (itemIndexToRemove > -1) {
      data.splice(itemIndexToRemove, 1);
      this.renderElements();
    }
  }

  /**
   * Send new request when user approaches to last bottom element
   * @param {*} lastRenderedElementIndex
   */
  shoudStartNewRequest(lastRenderedElementIndex) {
    const { data } = this.state;
    const min_elements_from_bottom = 15;

    return data.length - lastRenderedElementIndex < min_elements_from_bottom;
  }

  fetchData() {
    const { lastPageToken: pageToken } = this.state;
    this.state = { isFetching: true };

    return fetchMessages({ pageToken, limit: 50 })
      .then(response => this.onFetchComplete(response))
      .catch(error => this.onFetchError(error));
  }

  onFetchComplete(response) {
    this.state = {
      isFetching: false,
      data: this.state.data.concat(response.messages),
      lastPageToken: response.pageToken
    };
  }

  onFetchError(e) {
    this.state = { isFetching: false };

    // in case of first render just display message
    if (!this.state.data.length) {
      render(errorInfo(e), this.messagesListContainer);
    } else {
      // fetch data again when error occured
      // try to fetch again when it's fetching some new data
      setTimeout(() => {
        this.fetchData();
      }, 500);
    }
  }
}

export { App };
