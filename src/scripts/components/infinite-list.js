import requestAnimFrameThrottle from "../utils/requestAnimFrameThrottle";
import { outerHeight } from "../utils/dom-utils";

/**
 * This class implements infinite scrolling functionality. It takes container component, and
 * configuration including minimum and maximum number of nodes and render callback.
 *
 * This class listen on scroll event and invoke render callback with appropriate indexes with pointers to data items.
 *
 * Virtual scroll is similuated by calculating items height and manipulate with padding top and padding bottom.
 */
class InfiniteList {
  get containerElement() {
    return this._containerElement;
  }
  set containerElement(el) {
    this._containerElement = el;
  }

  get dataPointers() {
    return this._itemsPointers;
  }

  set dataPointers(pointers) {
    this._itemsPointers = Object.assign({}, this._itemsPointers, pointers);
  }

  constructor({
    containerElement,
    minNodesLength = 10,
    maxNodesLength = 25,
    renderHandler
  }) {
    this.containerElement = containerElement;
    this.minNodesLength = minNodesLength;
    this.maxNodesLength = maxNodesLength;
    this.renderHandler = renderHandler;
    this._lastScrollPos = 0;

    this.padding = {
      top: 0,
      bottom: 0
    };

    this.dataPointers = { firstItemIndex: 0, lastItemIndex: minNodesLength };

    this.initialRender();

    this.handleOnScroll = requestAnimFrameThrottle(
      this.handleOnScroll.bind(this)
    );
    this.attachVirtualScroll();
  }

  /**
   * Attach scroll event and maintain elements list if need
   */
  attachVirtualScroll() {
    window.addEventListener("scroll", this.handleOnScroll);
  }

  detachVirtualScroll() {
    window.removeEventListener("scroll", this.handleOnScroll);
  }

  initialRender() {
    this._triggerElementsUpdate();
  }

  /**
   * Invokes on scroll event
   */
  handleOnScroll() {
    const isScrollingDown = this._lastScrollPos < window.pageYOffset;
    const isScrollingUp = this._lastScrollPos > window.pageYOffset;

    if (isScrollingDown) {
      this.handleScrollingDown();
    }

    if (isScrollingUp) {
      this.handleScrollingUp();
    }

    this._lastScrollPos = window.pageYOffset;
  }

  /**
   * Users scrolls down - if necessary load new elements at the bottom of the list and remove old elements from bottom
   *
   * Updates data pointers and invoke render handler
   */
  handleScrollingDown() {
    while (this._shouldLoadElement()) {
      const firstItemIndex = this.dataPointers.firstItemIndex;
      this._incrementDataPointers();
      // when user scrolls down and first item index is incremented then we should remove previous index from DOM
      // this means that to keep scroll bar, we should add padding containing removed item outer height
      if (firstItemIndex < this.dataPointers.firstItemIndex) {
        const firstElHeight = outerHeight(
          this.containerElement.firstElementChild
        );
        const lastElHeight = outerHeight(
          this.containerElement.lastElementChild
        );

        this.padding.top += firstElHeight;
        this.padding.bottom -= lastElHeight;

        if (this.padding.bottom < 0) {
          this.padding.bottom = 0;
        }

        this.containerElement.style.paddingTop = this.padding.top + "px";
        this.containerElement.style.paddingBottom = this.padding.bottom + "px";
      }

      this.renderHandler(this.dataPointers);
    }

    this.renderHandler(this.dataPointers);
  }

  /**
   * Users scrolls up - if necessary add elements on the top of the list and remove elements from bottom,
   *
   * Updates data pointers and invoke render handler.
   */
  handleScrollingUp() {
    const lastItemIndex = this.dataPointers.lastItemIndex;

    while (
      this._shouldPrependElement() &&
      this.dataPointers.firstItemIndex > 0
    ) {
      this._decrementDataPointers();

      if (lastItemIndex > this.dataPointers.lastItemIndex) {
        const lastElHeight = outerHeight(
          this.containerElement.lastElementChild
        );
        this.padding.bottom += lastElHeight;

        this.containerElement.style.paddingBottom = this.padding.bottom + "px";
        this.renderHandler(this.dataPointers);

        // padding top should be updated after render becasue we want to update first rendered item padding
        this.padding.top -= outerHeight(
          this.containerElement.firstElementChild
        );

        if (this.padding.top < 0) {
          this.padding.top = 0;
        }

        this.containerElement.style.paddingTop = this.padding.top + "px";
      }
    }

    // make sure that when we scroll to the top, we always keep clean padding
    if (this.dataPointers.firstItemIndex === 0) {
      this.padding.top = 0;
      this.containerElement.style.paddingTop = this.padding.top + "px";
    }

    this.renderHandler(this.dataPointers);
  }

  /**
   * triggers render callback
   */
  _triggerElementsUpdate() {
    requestAnimFrameThrottle(this.renderHandler(this.dataPointers));
  }

  _incrementDataPointers() {
    let { lastItemIndex, firstItemIndex } = this.dataPointers;
    lastItemIndex = lastItemIndex + 1;

    const diff = lastItemIndex - firstItemIndex;
    const shouldIncrementStartIndex = diff > this.maxNodesLength - 1;

    this.dataPointers = {
      lastItemIndex,
      firstItemIndex: shouldIncrementStartIndex
        ? firstItemIndex + 1
        : firstItemIndex
    };
  }

  _decrementDataPointers() {
    let { lastItemIndex, firstItemIndex } = this.dataPointers;
    lastItemIndex--;
    firstItemIndex--;

    lastItemIndex = Math.max(lastItemIndex, this.minNodesLength);
    firstItemIndex = Math.max(firstItemIndex, 0);
    this.dataPointers = {
      lastItemIndex,
      firstItemIndex
    };
  }

  /**
   * Check if should add element at the top of the list
   */
  _shouldPrependElement() {
    const bounding = this.containerElement.firstElementChild.getBoundingClientRect();
    return bounding.top > -bounding.height;
  }

  /**
   * Check if loading new element is requried
   */
  _shouldLoadElement() {
    const bounding = this.containerElement.lastElementChild.getBoundingClientRect();
    return (
      bounding.bottom -
        (window.innerHeight || document.documentElement.clientHeight) <
      bounding.height * 2
    );
  }
}

export { InfiniteList };
