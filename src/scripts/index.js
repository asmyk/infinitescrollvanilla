import "reset-css";
import "../styles/index.scss"; 
import { App } from "./components/app";

document.addEventListener("DOMContentLoaded", function() { 
  new App();
});
