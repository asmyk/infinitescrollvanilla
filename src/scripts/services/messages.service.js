const API_URL = "http://message-list.appspot.com";
const API_METHOD = "messages";

/**
 * Request server for new messages
 */
function fetchMessages(options = { limit: "50", pageToken: "" }) {
  let params = ""; 
  if (options.limit) {
    params += `limit=${options.limit}&`;
  }

  if (options.pageToken) {
    params += `pageToken=${options.pageToken}`;
  }

  return fetch(`${API_URL}/${API_METHOD}?${params}`, {
    method: "GET"
  }).then(response => response.json());
}

export { fetchMessages };
