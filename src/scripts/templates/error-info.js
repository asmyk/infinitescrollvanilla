import { html } from "lit-html";

function errorInfo({ error }) {
  return html`
    <h2 class="mdc-typography ">Couldn't download data. Details ${error}</h2>
  `;
}
export { errorInfo };
