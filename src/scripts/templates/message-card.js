import { html } from "lit-html";
import { timeAgo } from "./time-ago";

/**
 * Single message card template component
 */
function messageCard({
  className = "",
  id = "",
  title = "",
  subtitle = "",
  message = "",
  img = ""
} = {}) {
  const date = html`
    ${timeAgo(subtitle)}
  `;

  return html`
    <div class="mdc-card message-card ${className}" data-message-id="${id}">
      <div class="message-card__header">
        <img
          class="message-card__avatar"
          src="http://message-list.appspot.com/${img}"
        />
        <div>
          <h2
            class="message-card__title mdc-typography mdc-typography--headline6"
          >
            ${title}
          </h2>
          <h3
            class="message-card__subtitle mdc-typography mdc-typography--subtitle2"
          >
            ${date}
          </h3>
        </div>
      </div>
      <div class="message-card__content mdc-typography--body2">
        ${message}
      </div>
    </div>
  `;
}

export { messageCard };
