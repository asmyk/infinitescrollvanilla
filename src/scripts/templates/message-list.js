import { html } from "lit-html";
import { repeat } from "lit-html/directives/repeat";
import detectIt from "detect-it";
import { messageCard } from "./message-card";
import {
  moveLeft,
  moveRight,
  setOpacity,
  restorePosition
} from "./../utils/dom-utils";
import requestAnimFrameThrottle from "../utils/requestAnimFrameThrottle";

/**
 * Message list component which renders array of messages by using message card template.
 * 
 * This also component handles swipe elements. When user try to swipe to right or to left, then releasing position
 * is checked. If position is more than half of available space then item is removed and callback is called. Otherwise,
 * element should back to default position.
 */
class MessageList {
  set state(newState) {
    this._state = Object.assign({}, this._state, newState);
  }
  get state() {
    return this._state;
  }

  constructor({ messages = [], onElementRemove = () => {} }) {
    this.messages = messages;
    this.messageItemClassSelector = "message-list--item";
    this.onElementRemove = onElementRemove;
    this.state = {};

    this._initTouchHandlers();
  }

  setMessages(messages) {
    this.messages = messages;
  }

  /**
   * Renders template
   */
  render() {
    return html`
      <div
        class="messages-list"
        @touchmove=${this.handleTouchmove}
        @touchend=${this.handleTouchend}
        @touchcancel=${this.handleTouchcancel}
        @touchstart=${this.handleTouchstart}
      >
        ${repeat(
          this.messages,
          message => message.id,
          message =>
            messageCard({
              className: this.messageItemClassSelector,
              id: message.id,
              title: message.author.name,
              subtitle: message.updated,
              message: message.content,
              img: message.author.photoUrl
            })
        )}
      </div>
    `;
  }

  onTouchStart(e) {
    const el = e.target.closest(`.${this.messageItemClassSelector}`);

    if (el) {
      this._setTouchingElementState(this._getElementId(el), {
        startX: e.targetTouches[0].clientX,
        startY: e.targetTouches[0].clientY,
        transition: getComputedStyle(el).transition
      });

      // disable previous transition for better touching experience
      el.style.transition = "none";
    }
  }

  onTouchMove(e) {
    const el = e.target.closest(`.${this.messageItemClassSelector}`);

    if (this._isSwippingCardElement(el)) {
      const { clientX, clientY } = e.targetTouches[0];
      const touchingState = this._getTouchingElementState(el.dataset.messageId);

      // calcualte diff from start position
      const xDiff = touchingState.startX - clientX;
      const yDiff = touchingState.startY - clientY;
      // update state for given element
      this._updateTouchingElementState(this._getElementId(el), {
        xDiff
      });

      // Make sure if we swipe element horizontally
      if (Math.abs(xDiff) > Math.abs(yDiff)) {
        if (xDiff > 0) {
          moveLeft(el, xDiff);
        } else {
          moveRight(el, xDiff);
        }
        setOpacity(el, this._getSwippedOpacity(el, xDiff));
      }
    }
  }

  onTouchEnd(e) {
    const el = e.target.closest(`.${this.messageItemClassSelector}`);

    if (this._isSwippingCardElement(el)) {
      const touchingState = this._getTouchingElementState(el.dataset.messageId);

      el.style.transition = touchingState.transition;
      // after user release
      if (this._shouldRemoveElement(el, touchingState.xDiff)) {
        this.handleRemoveElement(el);
      } else {
        setOpacity(el, 1);
        restorePosition(el);
      }
    }
  }

  handleRemoveElement(element) {
    const sectionHeight = element.scrollHeight;
    const handleTransitionFinish = () => {
      element.remove();
      this.onElementRemove(element.dataset.messageId);
      element.removeEventListener("transitionend", handleTransitionFinish);
    };

    let elementTransition = element.style.transition;
    element.style.transition = "";
    element.addEventListener("transitionend", handleTransitionFinish, {
      once: true
    });

    // Animate element swipe out from the screen
    requestAnimFrameThrottle(function() {
      element.style.height = sectionHeight + "px";
      element.style.transition = elementTransition;
      requestAnimFrameThrottle(function() {
        element.style.height = 0 + "px";
        element.style.opacity = 0;
      })();
    })();
  }

  _initTouchHandlers() {
    this.handleTouchmove = {
      handleEvent: e => requestAnimFrameThrottle(this.onTouchMove(e)),
      passive: !!detectIt.passiveEvents
    };
    this.handleTouchend = {
      handleEvent: e => requestAnimFrameThrottle(this.onTouchEnd(e)),
      passive: !!detectIt.passiveEvents
    };
    this.handleTouchcancel = {
      handleEvent: e => this.onTouchEnd(e),
      passive: !!detectIt.passiveEvents
    };
    this.handleTouchstart = {
      handleEvent: e => {
        this.onTouchStart(e);
      },
      passive: !!detectIt.passiveEvents
    };
  }

  _getElementId(el) {
    return el.dataset ? el.dataset.messageId : null;
  }

  _setTouchingElementState(id, newState) {
    this.state = { [id]: newState };
  }

  _updateTouchingElementState(id, newState) {
    this.state = { [id]: Object.assign({}, this.state[id], newState) };
  }

  _getTouchingElementState(id) {
    return this.state[id];
  }

  _isSwippingCardElement(el) {
    const { dataset: { messageId = "" } = {} } = el || {};
    return messageId && this.state[messageId];
  }

  _getSwippedOpacity(el, xOffset) {
    let ratio =
      (el.parentNode.offsetWidth - Math.abs(xOffset)) /
      el.parentNode.offsetWidth;
    return ratio / 1.2;
  }

  _shouldRemoveElement(el, xOffset) {
    return Math.abs(xOffset) > el.parentNode.offsetWidth / 2;
  }
}

export { MessageList };
