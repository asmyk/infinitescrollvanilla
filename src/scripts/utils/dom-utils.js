// get element height with margins
// necessary for padding space calculation
function outerHeight(el) {
  const width = el.offsetHeight;
  const style = getComputedStyle(el);

  return width + parseInt(style.marginTop) + parseInt(style.marginBottom);
}

function moveLeft(el, xOffset = 0) {
  el.style.transform = "translate3d(-" + xOffset + "px,0,0)";
}

function moveRight(el, xOffset = 0) {
  el.style.transform = "translate3d(" + Math.abs(xOffset) + "px,0,0)";
}

function restorePosition(el) {
  el.style.transform = "translate3d(0,0,0)";
}

function setOpacity(el, value = 1) {
  el.style.opacity = value;
}

export { outerHeight, moveLeft, moveRight, setOpacity, restorePosition };
